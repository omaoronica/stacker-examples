#!/usr/bin/env python
"""Module with vpc components."""
from __future__ import print_function

from troposphere import (
    Cidr, Equals, Export, GetAZs, Join, Output, Region, Select, Sub, Tags, ec2
)

from stacker.blueprints.base import Blueprint
from stacker.blueprints.variables.types import CFNNumber, CFNString

SUBNET_ZONES = ['Public', 'Private']


class Vpc(Blueprint):
    """Stacker blueprint for creating vpc components."""

    VARIABLES = {
        'EnvironmentName': {'type': CFNString,
                            'description': 'Environment/stage'},
        'CidrBlock': {'type': CFNString,
                      'allowed_pattern': '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))$',  # noqa pylint: disable=line-too-long
                      'description': 'IPv4 VPC CIDR (e.g. 172.16.24.0/21)'},
        'Ipv6MaskSize': {'type': CFNNumber,
                         'description': 'IPv6 subnet mask size',
                         'max_value': '128',
                         'min_value': '1',
                         'default': '64'},
        'DeployNatGateways': {'type': CFNString,
                              'description': 'Should NAT gateways be deployed '
                                             '(for ipv4 internet access from '
                                             'private subnets)',
                              'allowed_values': ['yes', 'no'],
                              'default': 'no'}
    }

    for cidrtype in SUBNET_ZONES:
        for i in range(1, 4):
            VARIABLES["%sSubnetCidr%i" % (cidrtype, i)] = {
                'type': CFNString,
                'allowed_pattern': '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\\/([0-9]|[1-2][0-9]|3[0-2]))$',  # noqa pylint: disable=line-too-long
                'description': 'IPv4 CIDR (e.g. 172.16.24.0/24) for subnet',
            }

    def create_template(self):  # pylint: disable=too-many-locals
        """Create template (main function called by Stacker)."""
        template = self.template
        variables = self.get_variables()
        template.add_version('2010-09-09')
        template.add_description('VPC')

        # Metadata
        template.add_metadata({
            'cfn-lint': {
                'config': {
                    'ignore_checks': [
                        # Complex use of Fn::Cidr appears to be tripping this
                        'E1024',
                        # https://github.com/cloudtools/troposphere/pull/1136
                        'E3012'
                    ]
                }
            }
        })

        # Conditions
        template.add_condition(
            'CreateNATGateways',
            Equals(variables['DeployNatGateways'].ref, 'yes')
        )

        # Resources
        vpc = template.add_resource(
            ec2.VPC(
                'Vpc',
                CidrBlock=variables['CidrBlock'].ref
            )
        )
        template.add_output(
            Output(
                'VPC',
                Description='VPC',
                Export=Export(Sub('${AWS::StackName}-VPC')),
                Value=vpc.ref()
            )
        )
        template.add_output(
            Output(
                'CidrBlock',
                Description='Set of IPv4 addresses for the VPC',
                Export=Export(Sub('${AWS::StackName}-CidrBlock')),
                Value=vpc.get_att('CidrBlock')
            )
        )

        ipv6cidr = template.add_resource(
            ec2.VPCCidrBlock(
                'Ipv6Cidr',
                AmazonProvidedIpv6CidrBlock=True,
                VpcId=vpc.ref()
            )
        )
        template.add_output(
            Output(
                'Ipv6CidrBlock',
                Description='IPv6 CIDR Block',
                Export=Export(Sub('${AWS::StackName}-Ipv6CidrBlock')),
                Value=Select(0, vpc.get_att('Ipv6CidrBlocks'))
            )
        )

        subnet_range = range(0, 3)

        subnets = {}
        for zone in SUBNET_ZONES:
            for i in subnet_range:
                subnets["%sSubnet%i" % (zone, (i+1))] = template.add_resource(  # noqa
                    ec2.Subnet(
                        "%sSubnet%i" % (zone, (i+1)),
                        DependsOn=ipv6cidr.title,
                        AssignIpv6AddressOnCreation=True,
                        AvailabilityZone=Select(i, GetAZs(Region)),
                        CidrBlock=variables["%sSubnetCidr%i" % (zone, (i+1))].ref,  # noqa
                        Ipv6CidrBlock=Select(
                            i if zone == 'Private' else i + len(subnet_range),
                            Cidr(Select(0, vpc.get_att('Ipv6CidrBlocks')),
                                 len(subnet_range)*len(SUBNET_ZONES),
                                 variables['Ipv6MaskSize'].ref)
                        ),
                        Tags=Tags(
                            Name=Join(
                                '-',
                                [zone.lower(),
                                 variables['EnvironmentName'].ref,
                                 Select(str(i), GetAZs(Region))]
                            )
                        ),
                        VpcId=vpc.ref()
                    )
                )
                template.add_output(
                    Output(
                        "%sSubnet%i" % (zone, (i+1)),
                        Description='Subnet Id',
                        Export=Export(
                            Sub('${AWS::StackName}-'
                                '%sSubnet%i' % (zone, (i + 1)))
                        ),
                        Value=subnets["%sSubnet%i" % (zone, (i+1))].ref()
                    )
                )

        egressonlygateway = template.add_resource(
            ec2.EgressOnlyInternetGateway(
                'EgressOnlyGateway',
                VpcId=vpc.ref()
            )
        )

        route_tables = {}
        for i in subnet_range:
            route_tables["PrivateSubnetsRouteTable%i" % (i + 1)] = template.add_resource(  # noqa
                ec2.RouteTable(
                    "PrivateSubnetsRouteTable%i" % (i + 1),
                    VpcId=vpc.ref(),
                    Tags=Tags(
                        Name="PrivateSubnetsRouteTable%i" % (i + 1)
                    )
                )
            )
            template.add_resource(
                ec2.Route(
                    "PrivateSubnet%iIPv6InternetRoute" % (i + 1),
                    DestinationIpv6CidrBlock='::/0',
                    EgressOnlyInternetGatewayId=egressonlygateway.ref(),
                    RouteTableId=route_tables["PrivateSubnetsRouteTable%i" % (i + 1)].ref()  # noqa
                )
            )
            template.add_resource(
                ec2.SubnetRouteTableAssociation(
                    "PrivateSubnet%iRTAssociation" % (i + 1),
                    SubnetId=subnets["PrivateSubnet%i" % (i + 1)].ref(),
                    RouteTableId=route_tables["PrivateSubnetsRouteTable%i" % (i + 1)].ref()  # noqa
                )
            )

        # Public subnets ipv4 internet access
        internetgateway = template.add_resource(
            ec2.InternetGateway(
                'InternetGateway',
            )
        )
        template.add_resource(
            ec2.VPCGatewayAttachment(
                'GatewayToInternet',
                InternetGatewayId=internetgateway.ref(),
                VpcId=vpc.ref(),
            )
        )
        publicroutetable = template.add_resource(
            ec2.RouteTable(
                'PublicRouteTable',
                VpcId=vpc.ref(),
                Tags=Tags(
                    Name='PublicRouteTable'
                )
            )
        )
        template.add_resource(
            ec2.Route(
                'PublicIPv4Route',
                DestinationCidrBlock='0.0.0.0/0',
                GatewayId=internetgateway.ref(),
                RouteTableId=publicroutetable.ref()
            )
        )
        template.add_resource(
            ec2.Route(
                'PublicIPv6Route',
                DestinationIpv6CidrBlock='::/0',
                GatewayId=internetgateway.ref(),
                RouteTableId=publicroutetable.ref()
            )
        )
        for i in subnet_range:
            template.add_resource(
                ec2.SubnetRouteTableAssociation(
                    "PublicSubnet%iRTAssociation" % (i + 1),
                    SubnetId=subnets["PublicSubnet%i" % (i + 1)].ref(),
                    RouteTableId=publicroutetable.ref()
                )
            )

        # Conditional NAT Gateway deployment
        nat_gateways = {}
        for i in subnet_range:
            nat_gateways["NAT%iElasticIP" % (i + 1)] = template.add_resource(
                ec2.EIP(
                    'NAT%iElasticIP' % (i + 1),
                    Condition='CreateNATGateways',
                    Domain='vpc'
                )
            )
            template.add_output(
                Output(
                    'NAT%iElasticIP' % (i + 1),
                    Condition='CreateNATGateways',
                    Description='Elastic IP for NATs %i' % (i + 1),
                    Export=Export(
                        Sub('${AWS::StackName}-NAT%iElasticIP' % (i + 1))
                    ),
                    Value=nat_gateways["NAT%iElasticIP" % (i + 1)].ref()
                )
            )
            nat_gateways["NATGateway%i" % (i + 1)] = template.add_resource(
                ec2.NatGateway(
                    'NATGateway%i' % (i + 1),
                    Condition='CreateNATGateways',
                    AllocationId=nat_gateways["NAT%iElasticIP" % (i + 1)].get_att(  # noqa
                        'AllocationId'
                    ),
                    SubnetId=subnets["PublicSubnet%i" % (i + 1)].ref(),
                    Tags=Tags(
                        Name="PrivateSubnet%iGateway" % (i + 1)
                    )
                )
            )
            template.add_resource(
                ec2.Route(
                    "PrivateSubnet%iIPv4InternetRoute" % (i + 1),
                    Condition='CreateNATGateways',
                    DestinationCidrBlock='0.0.0.0/0',
                    NatGatewayId=nat_gateways["NATGateway%i" % (i + 1)].ref(),
                    RouteTableId=route_tables["PrivateSubnetsRouteTable%i" % (i + 1)].ref()  # noqa
                )
            )


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context
    print(Vpc('test', Context({"namespace": "test"}), None).to_json())

