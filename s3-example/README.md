Run the following command:

For Runway:
DEPLOY_ENVIRONMENT=$environment_name runway deploy

For Stacker:
cd s3-example.cfn; stacker build -i $environment_name.env stacker.yaml
